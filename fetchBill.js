/*
This function is intended to be used for MongoDB stitch service. 
For more information refer : https://www.mongodb.com/cloud/stitch

Author: Ankit

For issues Raise ticket here

*/


let fetchBill = (function () {
    connection = function () {
        return context.services.get("mongodb-atlas").db("sandbox");
    }
    getBills = async function (query) {
        //get users and bills
        let db = connection();
        let result = await db.collection('users').aggregate([
            {
                '$match': {
                    'mobileNumber': query.mobileNumber
                }
            }, {
                '$lookup': {
                    from: 'bills',
                    // localField: 'mobileNumber',
                    // foreignField: 'customerAccount.id',
                    let: { mob: '$mobileNumber' },
                    pipeline: [
                        {
                            $match:
                            {
                                $expr:
                                {
                                    $and:
                                        [
                                            { $eq: ["$customerAccount.id", "$$mob"] },
                                            { $eq: ["$status", "0"] }
                                        ]
                                }
                            }
                        }
                    ],
                    as: 'bills'
                }
            }
        ]).toArray()

        if (result.length == 0) {
            return getResponse(0, {})
        }
        else if (result[0].bills.length == 0) {
            return getResponse(1, result[0])
        }
        else if (result[0].bills.length > 0) {
            return getResponse(2, result[0]);
        }
    }
    setResponse = function (code, type, body) {
        let returnObject = {
            code, success: type
        }
        return Object.assign(returnObject, body)
    }
    cleanBills = function (bills) {
        //Removing unnecessary data from response object 
        bills.forEach(function (item) {
            delete item._id;
            delete item.status;
        })
        return bills

    }
    getResponse = function (type, payload) {
        let response = {};
        switch (type) {
            case 0: return setResponse('404', 'false', {
                error: {
                    "code": "customer-not-found",
                    "title": "Customer not found",
                    "detail": "The requested customer was not found in the biller system.",
                    "traceID": "",
                    "docURL": ""
                }
            });
                break;
            case 1: return setResponse('200', true, {
                "data": {
                    "customer": {
                        "name": payload.name
                    },
                    "billDetails": {
                        "billFetchStatus": "NO_OUTSTANDING",
                        "bills": []
                    }
                }
            });
                break;
            case 2: return setResponse('200', true, {
                "data": {
                    "customer": {
                        "name": payload.name
                    },
                    "billDetails": {
                        "billFetchStatus": "AVAILABLE",
                        "bills": cleanBills(payload.bills)
                    }
                }
            })
                break;
        }

    }
    return {
        getBills
    }

})()

exports = async function (arg, token) {
    //token validation
    if (!token) {
        return {
            status: 401,
            success: 'false',
            error: {
                message: "Missing Authorization Token"
            }
        }
    }

    token = token.slice(7, token.length)

    try {
        let payload = utils.jwt.decode(token, '$$KEY$$')
        let aud = payload.aud;
        let iat = payload.iat;
        let epochTime = Math.floor(new Date() / 1000);
        let timeDiff = epochTime - iat;

        if (aud != "$$SCHEME_ID$$") {
            return {
                status: "401",
                success: 'false',
                error: {
                    message: "Invalid Scheme Id"
                }
            }
        }
        if (timeDiff > 120) {
            return {
                status: "401",
                success: 'false',
                error: {
                    message: "Expired Token"
                }
            }
        }
         //Binary input to MongoDB Stitch is encoded as EJSON
        const stringifiedExtendedJson = arg.text()
        const document = EJSON.parse(stringifiedExtendedJson)
        let number = document.customerIdentifiers[0].attributeValue;

        return await fetchBill.getBills({ mobileNumber: number });
    }
    catch (e) {
        return {
            status: "401",
            success: 'false',
            error: {
                message: "Invalid key"
            }
        }
    }
};