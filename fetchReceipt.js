/*
This function is intended to be used for MongoDB stitch service. 
For more information refer : https://www.mongodb.com/cloud/stitch

Author: Ankit

For issues Raise ticket here

*/


let reconcile = (function () {
    //Connection to DB managed by MongoDB Stitch context
    connection = function () {
        return context.services.get("mongodb-atlas").db("sandbox");
    }

    let getBill = async function (billId) {
        //find the bill
        let db = connection();
        return await db.collection('bills').find({ billerBillID: billId, status: '0' }).toArray();
    }
    let generateRandom = function () {
        return Math.floor((Math.random() * 100000) + 1);;
    }
    let recordPayment = async function (billId, value) {
        //record the document with paid value
        let db = connection();
        return await db.collection('bills').updateOne({ billerBillID: billId }, { $set: { 'status': "1" } })
    }
    let recordPartialPayment = async function (billId, newValue) {
        let db = connection();
        return await db.collection('bills').updateOne({ billerBillID: billId }, { $set: { 'aggregates.total.amount': String(newValue) } })
    }
    setResponse = function (code, type, body) {
        //frame the response object
        let returnObject = {
            code, success: type
        }
        return Object.assign(returnObject, body)
    }

    let recordReceipt = async function (receiptData) {
        //record the receipt and generate response
        let db = connection();
        return await db.collection('receipts').insertOne(receiptData)
    }

    let response = function (type, billData, value, document) {
        switch (type) {
            case 0: return setResponse('403', false, {
                error: {
                    message: 'Invalid Bill Id or bill already paid'
                }
            })
                break;
            case 1: return setResponse('200', 'true', {
                "data": {
                    "billerBillID": billData.billerBillID,
                    "platformBillID": document.platformBillID,
                    "platformTransactionRefID": document.paymentDetails.platformTransactionRefID,
                    "receipt": {
                        "id": 'R' + generateRandom(),
                        "date": (new Date()).toISOString(),
                    }

                }
            });

                break;
            case 2: return setResponse('403', false, {
                error: {
                    message: 'Total Payment exceeds the due'
                }
            })
                break;
            default: return setResponse('500', false, {
                error: {
                    message: 'Something went wrong!'
                }
            })

        }
    }
    processPayment = async function (billId, value, document) {
        //findbill
        let billdata;
        try {
           billdata  = await getBill(billId);
        }
        catch (e) {
            setResponse(4)
        }

        //validations
        if (billdata.length == 0) {
            return response(0)
        }
        //record payment

        let valueMatch = parseFloat(value) == parseFloat(billdata[0].aggregates.total.amount)
        if (!valueMatch) {
            //partial payment
            let billAmount = billdata[0].aggregates.total.amount;
            let newValue = parseFloat(billAmount) - parseFloat(value);
            if (newValue > 0) {
                recordPartialPayment(billdata[0].billerBillID, newValue);
                let receipt = response(1, billdata[0], value, document)
                recordReceipt(receipt.data);
                return receipt
            } else {
                return response(2)
            }

        }
        if (billdata.length > 0 && valueMatch) {
            //full payment
            try {
                recordPayment(billdata[0].billerBillID, value);
                let receipt = response(1, billdata[0], value, document)
                recordReceipt(receipt.data);
                return receipt
            } catch (e) {
                setResponse(4)
            }

        }


    }
    return { processPayment }
})()




exports = async function (arg, token) {
    //Token validation
    if (!token) {
        return {
            status: 401,
            success: 'false',
            error: {
                message: "Missing Authorization Token"
            }
        }
    }

    token = token.slice(7, token.length)

    try {
        let payload = utils.jwt.decode(token, '$$KEY$$')
        let aud = payload.aud;
        let iat = payload.iat;
        let epochTime = Math.floor(new Date() / 1000);
        let timeDiff = epochTime - iat;

        if (aud != "$$SCHEME-ID$$") {
            return {
                status: "401",
                success: 'false',
                error: {
                    message: "Invalid Scheme Id"
                }
            }
        }
        if (timeDiff > 120) {
            return {
                status: "401",
                success: 'false',
                error: {
                    message: "Expired Token"
                }
            }
        }
        //Binary input to MongoDB Stitch is encoded as EJSON
        const stringifiedExtendedJson = arg.text()
        const document = EJSON.parse(stringifiedExtendedJson)
        let billerBillID = document.billerBillID;
        let amount = document.paymentDetails.amountPaid.value;

        let payment = await reconcile.processPayment(billerBillID, amount, document)
        return payment

    }
    catch (e) {
        return {
            status: "401",
            success: 'false',
            error: {
                message: "Invalid Key"
            }
        }
    }
};
