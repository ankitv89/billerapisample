# Setu Biller Integration

Implementation of Setu Biller Integration based on [this docs](https://docs.setu.co/biller-quickstart).

Key Highlights
  - Implemented on MongoDB Stitch
  - Handles Partial Payments
 

### Why MongoDb stitch?

  - Database used was MongoDB Sandbox so it has out of box integration
  - No dev-ops required so easy to prototype and develop solutions


Replace the `$$KEY$$` and `$$SCHEME_ID$$` in the token auth functions before deploying.

Auth function was repeated instead of reusing as each MongoDB stitch function is isolated from each other. A prod grade solution will be develop a util library and re-use it as required.
